# coding: utf8
from __future__ import absolute_import
from aip import AipSpeech
from asr.configs import AUDIO_OUTPUT, AUDIO_FORMAT, APP_ID, API_KEY, SECRET_KEY
from asr.speech import audio_record
from asr.recognition import aip_get_asrresult
from asr.awake import up_ps_audio
from utils.log import Log
logger = Log().logger


def listening():
    audio_record(AUDIO_OUTPUT, 3)
    asr_result = aip_get_asrresult(AipSpeech(APP_ID, API_KEY, SECRET_KEY),
                                   AUDIO_OUTPUT,
                                   AUDIO_FORMAT)
    return "".join(asr_result) if asr_result else None

def wakeup(keywords: list):
    audio_record(AUDIO_OUTPUT, 3)
    word = up_ps_audio(AUDIO_OUTPUT)
    logger.info(f"唤醒>>> {word}")
    return (str(word) in keywords)

if __name__ == "__main__":
    print(listening())
