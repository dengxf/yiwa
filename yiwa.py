# coding: utf8

from asr.stt import listening, wakeup
from asr.awake import read_keywords
from nlp.comparison import match
from importlib import import_module
from web import HOST, PORT
import time
from utils.db import select
from utils.browser import create as create_browser
from utils.log import Log

logger = Log().logger

ROOT = f"http://{HOST}:{PORT}"


def get_commands():
    sql = "SELECT commands, action FROM commands"
    return select(sql)


if __name__ == "__main__":
    WAKEUP = True  # False
    FAILURE = 0
    keywords = read_keywords()
    browser = create_browser()


    def _todo(browser, action: str):
        """做action对应的动作"""
        mothed_path = action.split(".")
        package_path = ".".join(mothed_path[:-1])
        mothed_name = mothed_path[-1]
        if package_path and mothed_name:
            try:
                package = import_module(package_path)
                todo = package.__getattribute__(mothed_name)
                todo(browser)
            except Exception as e:
                logger.error(f"动态执行页面动作失败：{e}")


    def _exec():
        """执行指令"""
        access = False  # 成功执行指令
        voice2text = listening()
        logger.info(f"指令>>> {voice2text}")
        print(f"指令>>> {voice2text}")
        if voice2text is None:
            logger.info("空指令")
            return access

        for commands, action in get_commands():
            found = False  # 指令是否已找到
            for command in commands.split(","):
                if match(voice2text, command):
                    if action.startswith("/"):
                        # 页面访问
                        browser.get(ROOT + action)
                    else:
                        # 页面动作
                        _todo(browser, action)
                    found = True
                    break
            if found:
                access = True
                break
        else:
            logger.info("指令不匹配")
        return access


    while True:
        if WAKEUP:
            if _exec():
                FAILURE = 0
            else:
                FAILURE += 1
        else:
            if wakeup(keywords):
                logger.info("唤醒成功")
                if _exec():
                    FAILURE = 0
                else:
                    FAILURE += 1
                WAKEUP = True
        # 10次超时， 睡眠
        if FAILURE >= 10:
            WAKEUP = False
            FAILURE = 0
            logger.info("睡眠待命")

        time.sleep(1)
    browser.close()
